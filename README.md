# riscof-plugins

Contains various model plugins to be used along with RISCOF (https://gitlab.com/incoresemi/riscof)

Each plugin contains a `README.md` file for details on setup, usage and configuration.
